var app = angular.module('fileUpload', ['ngFileUpload']);

app.controller('MyCtrl', ['$scope', 'Upload', '$timeout', function ($scope, Upload, $timeout) {
    $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        console.log($scope.f);
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                 url: 'http://localhost:8080/api/v1/importa/salva_linha',
                //url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                data: {file: file}
            });
            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    console.log(file.result);
                    console.log(file);
                });
            }, function (response) {
                if (response.status > 0) { //Entra se houver algum erro no envio
                    $scope.errorMsg = response.status + ': ' + response.data;
                    console.log(response.status);
                }
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                                         evt.loaded / evt.total));
            });
        }
    }
}]);
