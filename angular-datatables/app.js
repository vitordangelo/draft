var dtTable = angular.module('myModule', ['datatables']);

dtTable.controller('MyCtrl', MyCtrl);

function MyCtrl($q, DTOptionsBuilder) {
    var vm = this;
    vm.dtOptions = DTOptionBuilder.newOptions()
    .withOptions('autoWidth', fnThatReturnsAPromise);

    function fnThatReturnsAPromise() {
        var defer = $q.defer();
        defer.resolve(false);
        return defer.promise;
    }
}
