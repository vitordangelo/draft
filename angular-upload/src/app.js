angular
    .module('app', ['angularFileUpload'])
    .controller('AppController', function($scope, FileUploader) {
        $scope.uploader = new FileUploader({
            url: 'localhost:8000/api/v1/importa/salva_linha'
        });
    });
