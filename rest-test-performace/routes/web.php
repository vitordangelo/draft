<?php

Route::group(['prefix'=>'api/v1'], function(){
	Route::resource('users', 'UserController');
});

Route::get('/', function () {
    return view('welcome');
});
