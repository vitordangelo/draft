A Pen created at CodePen.io. You can find this one at http://codepen.io/monkeyraptor/pen/QbMNPB.

 There's the restriction (security related) here if the image source is taken from external URL. The override [crossOrigin] attribute won't work for image served from a different domain. That works only for same domain.

For local file, it works just fine.

Reference for input[type="file"] handler and binding it to canvas: http://stackoverflow.com/a/10906961

Reference for the "tainted canvas" restriction:
https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_enabled_image

LOCAL FILES:
This method can be used to convert just about anything into base64 URI (local files).
Most document types, executable files, compressed files, CSS files, JavaScript files, font files, audio, and video can be converted into base64 URI.
Basically any file types that a browser can display.

But then again, there's that "vague" limit of URI length a browser can accommodate (the characters on the address bar).
So the common restriction is the size.
It's varied between browsers.

For the self-running (executable) files conversion, any browser by default will give warning. Maybe. Well, they should.

Documentation about FileReader():
https://developer.mozilla.org/en/docs/Web/API/FileReader

For the ready to use tool:
http://portraptor.johanpaul.net/2015/08/base64-image-converter.html