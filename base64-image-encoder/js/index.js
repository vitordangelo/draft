//Updated the explanation and the coding below on Sept 23, 2015.
(function (d) {
    "use strict";
    //Elements/objects global variables
    var inp, out, note, notee, st, sut, iu, su, ch, aw, canvas, c2d;

    //Get element(s) (by Id or Class)
    function q(a) {        
        var buff;
        if (!(/^\./.test(a))) {
            if (d.getElementById(a)) {
                buff = d.getElementById(a);
            } else {
                buff = d.querySelector(a);
            }
        } else {
            buff = Array.prototype.slice.call(d.querySelectorAll(a));
        }
        return buff;
    }

    //IMG TESTER
    function imt(a) { // test the local file's tail-strings (allowed image format/extension)
        return (/jpe?g|gif|png|bmp|webp|svg/i).test(a);
    }

    //ELEMENTS/OBJECTS VARS INIT
    inp = q("inp"); //input file
    out = q("output_url"); //the base64 URI output text
    note = q("note"); //local file notification
    notee = q("notee"); //external url notification
    st = q("st"); //the "select"
    sut = q("sub_title"); //the bottom notification
    iu = q("inp_url"); //external url input
    su = q("submit_url"); //external url button
    ch = q(".inner_holder"); //inner holders (plural)
    aw = q(".all_wrapper")[0]; //outer holder
    canvas = q("imageCanvas"); //the canvas
    //[canvas] related
    c2d = canvas.getContext("2d");

    //CHECK HTML5 COMPATIBILITY
    (function () {
        aw.style.display = "block";
        if ((typeof canvas !== "object") && (typeof FileReader !== "function")) {
            aw.innerHTML = "<span style='font-size:24px'>Your browser doesn't support <strong>HTML5</strong> built-in API.</span>";
            //all functions below will have no triggers, because the "all_wrapper" content is changed to be just that "span" above.
        }
    }());

    //COMMON HANDLERS
    function loadThis(a) {
        canvas.width = a.width;
        canvas.height = a.height;
        c2d.drawImage(a, 0, 0);
    }
    function showElms() {
        ch.forEach(function (v) {
            v.style.display = "block";
        });
        canvas.style.opacity = 1;
    }
    function hideElms() {
        ch.forEach(function (v) {
            v.style.display = "none";
        });
        out.removeAttribute("style");
        out.value = "";
        inp.value = "";
        canvas.removeAttribute("style");
        st.removeAttribute("style");
        sut.removeAttribute("style");
        c2d.clearRect(0, 0, canvas.width, canvas.height);
        note.innerHTML = note.getAttribute("data-title");
        notee.innerHTML = notee.getAttribute("data-title");
        if (out.getAttribute("data-text")) {
            out.removeAttribute("data-text");
        }
        su.disabled = 1;
    }

    //EXTERNAL URL CHECKER
    function check_img_URL(a, b) {
        var protocol = /^https?:\/\//.test(a.value),
            btn = q(b);
        //RESET LOCAL INPUT AND OUTPUT
        if (inp.value.length) {
            inp.value = "";
        }
        note.innerHTML = note.getAttribute("data-title");
        a.removeAttribute("style");
        hideElms();
        //CHECK URL STARTS HERE
        if (protocol && a.value.length > 20 &&
                a.value.match(/\//g).length > 2 &&
                a.value.match(/\./g).length > 1 &&
                imt(a.value)) {
            btn.disabled = 0;
        } else if (a.value.length === 0) {
            hideElms();
        } else {
            a.style.color = "brown";
            btn.disabled = 1;
        }
    }

    //EXTERNAL URL HANDLER (BUTTON CLICK)
    function handle_img_URL() {
        var img_url = iu.value,
            new_img = d.createElement("img"),
            word;
        new_img.src = img_url;
        new_img.addEventListener("load", function () {
            if (new_img.complete) {
                try { //umm...
                    loadThis(new_img); //draw the image to [canvas].
                    out.value = c2d.getImageData(0, 0, new_img.width, new_img.height).data; //get URI.
                    notee.innerHTML = "Done!"; //not really.
                } catch (error) { //this'd certainly happen.
                    if (error) {
                        word = error.toString();
                        word = word.substring(0, word.indexOf(":"));
                        out.setAttribute("data-text", "<span style='color:red'>" + word + "</span>");
                        out.value = error.message;
                        out.style.color = "purple";
                        if (window.innerWidth > 1024) {
                            out.style.overflowX = "hidden";
                        }
                        st.innerHTML = "<span style='color:red'>" + word + "</span>";
                        sut.style.display = "none";
                        notee.innerHTML = "<span style='color:violet'>" + word + "</span>";
                    }
                }
                su.disabled = 1;
                showElms();
            }
        }, 0);
    }

    //LOCAL FILE HANDLER
    function handleImage(e) { // http://stackoverflow.com/a/10906961
        //the "e" is the "change" event object from the input[type='file'].
        //you can console.log(e) to find out the what's in it.
        var reader = new FileReader(),
            theFile = e.target.files[0]; //the file object being loaded.
        try {
            if (imt(theFile.type)) { //test if it has the allowed format, using imt() function - declared above.
                //to get the file size, use [size] key                
                //as in: theFile.size (using the variable "theFile") <- it's in Bytes (number).
                //to get the file name, use [name] key
                //example: theFile.name <- returns string
                reader.readAsDataURL(theFile);
                //wait for the image to load.
                reader.addEventListener("load", function (ev) {
                    //the "ev" parameter is the "reader" variable load event object.
                    //you can console.log(ev) to find out what's in it.
                    var img = d.createElement("img"); //create [img] object.
                    //the conversion product is right here [ev.target.result]
                    img.src = ev.target.result; //the [img] source -> base64 URI strings.
                    //And then listen to the load event of that [img] object.
                    img.addEventListener("load", function () {
                        loadThis(img); //draw that [img] to the [canvas].
                    }, 0);
                    out.value = img.src; //base64 URI strings.

                    note.innerHTML = "Done!"; //notification.
                    st.innerHTML = "Click below to select"; //notification.
                }, 0);
            } else {
                window.alert("Only jpg, jpeg, gif, png, bmp, webp, and svg");
                hideElms();
                return;
            }
        } catch (error) {
            if (error) { // This happens when we cancel the file window opener.
                hideElms();
                return;
            }
        }
        out.removeAttribute("style");
        showElms();
    }

    //EVENT LISTENERS
    su.onclick = handle_img_URL;
    inp.onclick = function () {
        hideElms();
        iu.value = "";
    };
    inp.onchange = handleImage;
    iu.oninput = function () {
        check_img_URL(iu, "submit_url");
    };
    iu.onclick = function () {
        iu.select();
    };
}(document));
//This demo uses the built-in input[type="file"] display with subtle CSS override.
//This is a neat snippet by Chris Coyier for styling that exciting HTML5 element: https://css-tricks.com/snippets/css/custom-file-input-styling-webkitblink/