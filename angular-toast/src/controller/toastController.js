angular.module('myApp').controller('ToastController', function($scope, ngToast) {
    $scope.name = "Vitor Ivan D'Angelo";
    $scope.default = function() {
        ngToast.create({
            className: 'success',
            content: '<a href="#" class="success">a Esta é uma mensagem de teste</a>'
        });

        var myToastMsg = ngToast.success({
            content: '<a href="#" class="">a message</a>'
        });

        console.log("Entrou");
    }
});
