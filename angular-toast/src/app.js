var app = angular.module('myApp', ['ngToast']);

app.config(['ngToastProvider', function(ngToastProvider) {
    ngToastProvider.configure({
        animation: 'slide' // or 'fade'
    });
}]);
